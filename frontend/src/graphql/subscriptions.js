/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateProducts = /* GraphQL */ `
  subscription OnCreateProducts(
    $id: ID
    $productName: String
    $reference: String
    $weight: Float
    $category: String
  ) {
    onCreateProducts(
      id: $id
      productName: $productName
      reference: $reference
      weight: $weight
      category: $category
    ) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const onUpdateProducts = /* GraphQL */ `
  subscription OnUpdateProducts(
    $id: ID
    $productName: String
    $reference: String
    $weight: Float
    $category: String
  ) {
    onUpdateProducts(
      id: $id
      productName: $productName
      reference: $reference
      weight: $weight
      category: $category
    ) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const onDeleteProducts = /* GraphQL */ `
  subscription OnDeleteProducts(
    $id: ID
    $productName: String
    $reference: String
    $weight: Float
    $category: String
  ) {
    onDeleteProducts(
      id: $id
      productName: $productName
      reference: $reference
      weight: $weight
      category: $category
    ) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const onCreateCompanyTable = /* GraphQL */ `
  subscription OnCreateCompanyTable($id: String) {
    onCreateCompanyTable(id: $id) {
      id
    }
  }
`;
export const onUpdateCompanyTable = /* GraphQL */ `
  subscription OnUpdateCompanyTable($id: String) {
    onUpdateCompanyTable(id: $id) {
      id
    }
  }
`;
export const onDeleteCompanyTable = /* GraphQL */ `
  subscription OnDeleteCompanyTable($id: String) {
    onDeleteCompanyTable(id: $id) {
      id
    }
  }
`;
