/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getProducts = /* GraphQL */ `
  query GetProducts($id: ID!) {
    getProducts(id: $id) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const listProducts = /* GraphQL */ `
  query ListProducts(
    $filter: TableProductsFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProducts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        productName
        reference
        weight
        category
        stock
        dateCreate
        lastSale
      }
      nextToken
    }
  }
`;
export const getCompanyTable = /* GraphQL */ `
  query GetCompanyTable($id: String!) {
    getCompanyTable(id: $id) {
      id
    }
  }
`;
export const listCompanyTables = /* GraphQL */ `
  query ListCompanyTables(
    $filter: TableCompanyTableFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCompanyTables(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
      }
      nextToken
    }
  }
`;
