/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createProducts = /* GraphQL */ `
  mutation CreateProducts($input: CreateProductsInput!) {
    createProducts(input: $input) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const updateProducts = /* GraphQL */ `
  mutation UpdateProducts($input: UpdateProductsInput!) {
    updateProducts(input: $input) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const deleteProducts = /* GraphQL */ `
  mutation DeleteProducts($input: DeleteProductsInput!) {
    deleteProducts(input: $input) {
      id
      productName
      reference
      weight
      category
      stock
      dateCreate
      lastSale
    }
  }
`;
export const createCompanyTable = /* GraphQL */ `
  mutation CreateCompanyTable($input: CreateCompanyTableInput!) {
    createCompanyTable(input: $input) {
      id
    }
  }
`;
export const updateCompanyTable = /* GraphQL */ `
  mutation UpdateCompanyTable($input: UpdateCompanyTableInput!) {
    updateCompanyTable(input: $input) {
      id
    }
  }
`;
export const deleteCompanyTable = /* GraphQL */ `
  mutation DeleteCompanyTable($input: DeleteCompanyTableInput!) {
    deleteCompanyTable(input: $input) {
      id
    }
  }
`;
