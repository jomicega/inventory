import ApolloClient from "apollo-boost";

export const client = new ApolloClient({
  uri:
    "https://7vvmj3cka5ctbcxgy3xkefb6ne.appsync-api.us-east-1.amazonaws.com/graphql",
});
