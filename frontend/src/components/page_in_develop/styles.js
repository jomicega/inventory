import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles({
    container: {
        margin: 50,
        marginTop:100,
        textAlign: 'center'
    }
})