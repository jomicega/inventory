import React from 'react'
import { Paper } from '@material-ui/core'

import { useStyles } from './styles'

const PageInDevelop = ({ pageName }) => {
    const classes = useStyles()
    return (
        <Paper className={classes.container} elevation={5}>
            <div>
                Página en desarrollo
            </div>
            <div>
                {pageName}
            </div>
            <div>
                Estará disponible próximamente
            </div>
        </Paper>
    )
}

export default PageInDevelop