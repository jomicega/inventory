import React, { useState, useEffect } from "react";
import _ from "lodash";
import { API, graphqlOperation } from "aws-amplify";
import {
  Paper,
  Table,
  TableCell,
  TableHead,
  TableBody,
  TableRow,
  IconButton,
} from "@material-ui/core";
import { Edit, Delete } from "@material-ui/icons";
import Typography from "@material-ui/core/Typography";
import { useStyles } from "../page_in_develop/styles";
import { listProducts } from "../../graphql/queries";

const Dashboard = () => {
  const classes = useStyles();

  useEffect(() => {
    getList();
  }, []);

  const [list, setList] = useState([]);

  async function getList() {
    const getList = await API.graphql(graphqlOperation(listProducts));
    const myList = _.get(getList, "data.listProducts.items", []);
    setList(myList);
  }

  return (
    <Paper className={classes.container}>
      <Typography variant="h3">Lista de productos</Typography>
      <Table>
        <TableHead>
          <TableCell>Id</TableCell>
          <TableCell align="right">Nombre</TableCell>
          <TableCell align="right">Precio</TableCell>
          <TableCell align="right">Peso</TableCell>
          <TableCell align="right">Unidades disponibles</TableCell>
          <TableCell align="right">Última venta</TableCell>
          <TableCell align="right"></TableCell>
          <TableCell align="right"></TableCell>
        </TableHead>
        <TableBody>
          {list.map((product) => {
            const { id, productName, price, weight, stock, lastSale } = product;
            return (
              <TableRow>
                <TableCell>{id}</TableCell>
                <TableCell align="right">{productName}</TableCell>
                <TableCell align="right">{price}</TableCell>
                <TableCell align="right">{weight}</TableCell>
                <TableCell align="right">{stock}</TableCell>
                <TableCell align="right">{lastSale}</TableCell>
                <TableCell align="right">
                  <IconButton onClick={() => alert(JSON.stringify(product))}>
                    <Edit />
                  </IconButton>
                </TableCell>
                <TableCell align="right">
                  <IconButton onClick={() => console.log(product)}>
                    <Delete />
                  </IconButton>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default Dashboard;
