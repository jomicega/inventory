import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import clsx from 'clsx';
import {
    useTheme
} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useStyles } from './styles'
import Dashboard from '../dashboard'

export default function App() {
    const classes = useStyles()
  const theme = useTheme();

    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };
    return (
        <>
            <CssBaseline />
                <AppBar
                    position = "fixed"
                    className = {
                        clsx(classes.appBar, {
                            [classes.appBarShift]: open,
                        })
                    } >
                <Toolbar >
                    <IconButton
                        color = "inherit"
                        aria-label = "open drawer"
                        onClick = {
                            handleDrawerOpen
                        }
                        edge = "start"
                        className = {
                                clsx(classes.menuButton, open && classes.hide)
                            } >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant = "h6" noWrap >
                        Konecta Test
                        </Typography> 
                    </Toolbar> 
            </AppBar>
            <Drawer
                className = {classes.drawer}
                variant = "persistent"
                anchor = "left"
                open = {open}
                classes = {{paper: classes.drawerPaper }}
            >
                <div className = {classes.drawerHeader } >
                    <IconButton onClick={handleDrawerClose} >
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />} 
                    </IconButton> 
                </div>
                <Divider />
                <List>
                    <ListItem>
                        <ListItemIcon></ListItemIcon>
                        <ListItemText>Listar productos</ListItemText>
                    </ListItem>
                </List>
                <Divider />
            </Drawer>
            <Router >
            <Switch >
                <Route path = "/" >            
                <Dashboard />
            </Route>
        </Switch> 
            </Router>
            </>
    );
}
