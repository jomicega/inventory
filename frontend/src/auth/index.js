import Amplify from "aws-amplify";
import { withAuthenticator } from "@aws-amplify/ui-react";

import amplify from "../aws-exports";

Amplify.configure({
  ...amplify,
  Analytics: {
    disabled: true,
  },
});

function configureAmplify(App) {
  return withAuthenticator(App);
}

export default configureAmplify;
