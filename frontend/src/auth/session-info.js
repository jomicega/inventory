import React, { createContext, useEffect, useState } from "react";
import _ from "lodash";
import { Auth } from "aws-amplify";

const UserContext = createContext(null);

export function UserProvider({ children }) {
  const [userInfo, setUserInfo] = useState("");

  useEffect(() => {
    Auth.currentAuthenticatedUser().then((values) => {
      const username = _.get(values, "username", null);
      const userCognitoInfo = _.get(values, "attributes", null);
      const finalUserInfo = _.assign({ username }, userCognitoInfo);
      setUserInfo(finalUserInfo);
    });
  });

  return (
    <UserContext.Provider value={userInfo}>{children}</UserContext.Provider>
  );
}
