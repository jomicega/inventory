import React from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import { client } from "./client";
import { UserProvider } from "./auth/session-info";
import configureAuth from "./auth";
import NavigatorApp from "../src/components/router";

function App() {
  return (
    <ApolloProvider client={client}>
      <UserProvider>
        <NavigatorApp />
      </UserProvider>
    </ApolloProvider>
  );
}

export default configureAuth(App);
